package com.spring.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
public class Users implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(length = 50, nullable = false, name = "user_name")
    private String username;
    @Column(length = 32, nullable = false)
    private String password;

    @Column(length = 200)
    private String avatar;

}
