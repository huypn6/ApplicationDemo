package com.spring.model;

import lombok.Data;

@Data
public class UserInfo {

    private Integer id;
    private String username;
    private String avatar;

}
