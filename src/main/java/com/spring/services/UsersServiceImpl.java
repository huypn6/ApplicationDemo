package com.spring.services;

import com.spring.entity.Users;
import com.spring.model.UserInfo;
import com.spring.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UsersServiceImpl implements UsersService {

    @Autowired
    UserRepository userRepository;

    @Override
    public List<UserInfo> readAll() {
        List<UserInfo> result = new ArrayList<>();
        List<Users> list =  userRepository.findAll();

        for (Users u : list) {
            UserInfo userInfo = new UserInfo();
            userInfo.setId(u.getId());
            userInfo.setUsername(u.getUsername());
            userInfo.setAvatar(u.getAvatar());

            result.add(userInfo);
        }
        return result;
    }
}
