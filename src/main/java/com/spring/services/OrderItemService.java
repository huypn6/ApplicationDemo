package com.spring.services;

import com.spring.model.OrderItemModel;

import java.util.List;

public interface OrderItemService {

    List<OrderItemModel> getAll();

    OrderItemModel findById(Integer id);

    OrderItemModel insert(OrderItemModel model);

}
