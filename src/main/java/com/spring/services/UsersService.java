package com.spring.services;

import com.spring.model.UserInfo;

import java.util.List;

public interface UsersService {

    List<UserInfo> readAll();
}
