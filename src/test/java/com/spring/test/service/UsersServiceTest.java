package com.spring.test.service;

import com.spring.model.UserInfo;
import com.spring.services.UsersService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class UsersServiceTest {

	@Autowired
	UsersService usersService;

	@Test
	void loadAllUser() {
		List<UserInfo> users = usersService.readAll();
		System.out.println(users);
	}

}
